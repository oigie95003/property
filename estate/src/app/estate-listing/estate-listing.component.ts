import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { CribsService } from './../services/cribs.service';

import { UtilService } from '../services/util.service';
@Component({
  selector: 'app-estate-listing',
  templateUrl: './estate-listing.component.html',
  styleUrls: ['./estate-listing.component.css']
})
export class EstateListingComponent implements OnInit {
   cribs:Array<any>;
   error:string;
   sortFields: Array<string> = [
     'address', 
     'area', 
     'bathroom',
     'bedrooms',
     'price',
     'type'
    ];
   
   constructor(
    private http: Http, 
    private cribsService: CribsService,
    private utilService: UtilService) { }
   
  ngOnInit() {
    
    this.cribsService.getAllCribs()
    .subscribe(
      data => this.cribs = data,
      error => this.error = error.statusText
    );

    this.cribsService.newCribSubject.subscribe(
      data => console.log(data)
    )
  }

}
