/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EstateListingComponent } from './estate-listing.component';

describe('EstateListingComponent', () => {
  let component: EstateListingComponent;
  let fixture: ComponentFixture<EstateListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstateListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstateListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
